<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150705120621 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prices (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, currency VARCHAR(10) NOT NULL, offer NUMERIC(8, 2) NOT NULL, date DATETIME NOT NULL, yield NUMERIC(4, 2) NOT NULL, citicode VARCHAR(10) NOT NULL, cedol VARCHAR(15) NOT NULL, icin VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE funds ADD prices_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE funds ADD CONSTRAINT FK_6654D51D9C9DE39 FOREIGN KEY (prices_id) REFERENCES prices (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6654D51D9C9DE39 ON funds (prices_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE funds DROP FOREIGN KEY FK_6654D51D9C9DE39');
        $this->addSql('DROP TABLE prices');
        $this->addSql('DROP INDEX UNIQ_6654D51D9C9DE39 ON funds');
        $this->addSql('ALTER TABLE funds DROP prices_id');
    }
}
