<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150705151129 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE investments (id INT AUTO_INCREMENT NOT NULL, c_initial NUMERIC(3, 2) NOT NULL, annual NUMERIC(3, 2) NOT NULL, ocf NUMERIC(3, 2) NOT NULL, m_initial INT NOT NULL, additional INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE funds ADD investment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE funds ADD CONSTRAINT FK_6654D516E1B4FD5 FOREIGN KEY (investment_id) REFERENCES investments (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6654D516E1B4FD5 ON funds (investment_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE funds DROP FOREIGN KEY FK_6654D516E1B4FD5');
        $this->addSql('DROP TABLE investments');
        $this->addSql('DROP INDEX UNIQ_6654D516E1B4FD5 ON funds');
        $this->addSql('ALTER TABLE funds DROP investment_id');
    }
}
