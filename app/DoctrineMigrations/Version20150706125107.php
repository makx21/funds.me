<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150706125107 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE information (id INT AUTO_INCREMENT NOT NULL, legal_struct VARCHAR(255) NOT NULL, isa_own TINYINT(1) DEFAULT \'0\' NOT NULL, trustee VARCHAR(255) NOT NULL, registrar VARCHAR(255) NOT NULL, m_manager TINYINT(1) DEFAULT \'0\' NOT NULL, dividend_policy LONGTEXT NOT NULL, fund_size LONGTEXT NOT NULL, unit_launch DATE NOT NULL, fund_launch DATE NOT NULL, save_plan TINYINT(1) DEFAULT \'0\' NOT NULL, pricing_time VARCHAR(255) NOT NULL, dealing_times VARCHAR(255) NOT NULL, dealing_frequency VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE funds ADD info_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE funds ADD CONSTRAINT FK_6654D515D8BC1F8 FOREIGN KEY (info_id) REFERENCES information (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6654D515D8BC1F8 ON funds (info_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE funds DROP FOREIGN KEY FK_6654D515D8BC1F8');
        $this->addSql('DROP TABLE information');
        $this->addSql('DROP INDEX UNIQ_6654D515D8BC1F8 ON funds');
        $this->addSql('ALTER TABLE funds DROP info_id');
    }
}
