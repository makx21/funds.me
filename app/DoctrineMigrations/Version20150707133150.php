<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150707133150 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dividends (id INT AUTO_INCREMENT NOT NULL, fund_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, amount VARCHAR(255) NOT NULL, tax VARCHAR(255) NOT NULL, dividend_date DATE NOT NULL, payment_date DATE NOT NULL, INDEX IDX_62FF7AA625A38F89 (fund_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dividends ADD CONSTRAINT FK_62FF7AA625A38F89 FOREIGN KEY (fund_id) REFERENCES funds (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE dividends');
    }
}
