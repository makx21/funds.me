<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150710032329 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE investments CHANGE c_initial c_initial NUMERIC(4, 2) NOT NULL, CHANGE annual annual NUMERIC(4, 2) NOT NULL, CHANGE ocf ocf NUMERIC(4, 2) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE investments CHANGE c_initial c_initial NUMERIC(3, 2) NOT NULL, CHANGE annual annual NUMERIC(3, 2) NOT NULL, CHANGE ocf ocf NUMERIC(3, 2) NOT NULL');
    }
}
