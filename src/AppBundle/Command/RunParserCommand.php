<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RunParserCommand
 * @package AppBundle\Command
 */
class RunParserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setDescription('Funds parser')
            ->setName('parser:run');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('funds.parser_manager')->run(false);
        $output->writeln('Funds was parsed');
    }
}