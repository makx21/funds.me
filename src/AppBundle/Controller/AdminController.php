<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Fund;
use AppBundle\Entity\News;
use AppBundle\Form\Type\FundType;
use AppBundle\Form\Type\NewsType;
use AppBundle\Entity\Content;
use AppBundle\Entity\Investment;
use AppBundle\Entity\Information;
use AppBundle\Entity\Prices;

/**
 * Class AdminController
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $funds = $this->getDoctrine()->getRepository('AppBundle:Fund')->findAll();
        $form = $this->createForm(new FundType(), new Fund());
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $funds,
            $request->query->get('page', 1),
            50
        );

        return $this->render('AppBundle:Admin:index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newsAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository('AppBundle:News')->findAll();
        $form = $this->createForm(new NewsType(), new News());
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $news,
            $request->query->get('page', 1)
        );

        return $this->render('AppBundle:Admin:news.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createUpdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($id)) {
            $fund = $em->getRepository('AppBundle:Fund')
                ->find($id);

            if (null === $fund) {
                throw $this->createNotFoundException('Unable to find Fund with id '.$id);
            }
        } else {
            $fund = new Fund();
        }

        $form = $this->createForm(new FundType($request->get('n') || !is_null($id)), $fund);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);

            if ($form->isValid()) {
                $fund->setOur(1);
                $em->persist($fund);

                foreach($fund->getDividends() as $divivdend) {
                    $divivdend->setFund($fund);
                    $em->persist($divivdend);
                }

                $em->flush();
            }

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => $form->isValid() ? 'Fund was successfully '.($id ? 'edited' : 'created') : (string) $form->getErrors(),
                'status' => $form->isValid() ? 'success' : 'danger',
            ));


            return $this->redirectToRoute('funds_admin_homepage');
        }

        return $this->render('AppBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
        ));

    }

    /**
     * @param Fund $fund
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Fund $fund)
    {
        $this->getDoctrine()->getRepository('AppBundle:Fund')->removeFund($fund);

        $this->get('session')->getFlashBag()->add('funds-notice', array(
            'message' => 'Fund was successfully removed',
            'status' => 'success',
        ));

        return $this->redirect($this->generateUrl('funds_admin_homepage'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doParseAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('funds.parser_manager')->run($request->get('all'));

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'Funds was successfully parsed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('funds_admin_homepage'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doParseOneAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('funds.parser_manager')->runSpecificFund($request->get('code'));

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'Funds was successfully parsed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('funds_admin_homepage'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doUpdateCodesAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('funds.parser_manager')->updateRating();

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'Codes was successfully parsed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('funds_admin_homepage'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doNewsParseAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('funds.news_parser_manager')->run();

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'News was successfully parsed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('funds_admin_news'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $users,
            $request->query->get('page', 1)
        );

        return $this->render('AppBundle:Admin:index_users.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createUpdateNewsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($id)) {
            $news = $em->getRepository('AppBundle:News')
                ->find($id);

            if (null === $news) {
                throw $this->createNotFoundException('Unable to find News with id '.$id);
            }
        } else {
            $news = new News();
        }

        $form = $this->createForm(new NewsType(!is_null($id)), $news);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);

            if ($form->isValid()) {
                $em->persist($news);
                $em->flush();
            }

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => $form->isValid() ? 'News was successfully '.($id ? 'edited' : 'created') : (string) $form->getErrors(),
                'status' => $form->isValid() ? 'success' : 'danger',
            ));

            return $this->redirectToRoute('funds_admin_news');
        }

        return $this->render('AppBundle:Admin:edit_news.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
        ));

    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteNewsAction($id)
    {
        $newsRepo = $this->getDoctrine()->getRepository('AppBundle:News');

        $res = $newsRepo->deleteNews($id);

        if ($res) {
            if (in_array('hasImages', $res)) {
                $this->get('funds.s3')->delete($id);
            }
            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'News was successfully removed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('funds_admin_news'));
    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveUpAction($id)
    {
        $this->getDoctrine()->getRepository('AppBundle:News')->moveUpNewsPosition($id);

        return $this->redirect($this->generateUrl('funds_admin_news'));
    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveDownAction($id)
    {
        $this->getDoctrine()->getRepository('AppBundle:News')->moveDownNewsPosition($id);

        return $this->redirect($this->generateUrl('funds_admin_news'));
    }

}
