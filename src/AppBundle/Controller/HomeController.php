<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Fund;

/**
 * Class HomeController
 * @package AppBundle\Controller
 */
class HomeController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $funds = $this->getDoctrine()->getManager()->getRepository('AppBundle:Fund')->findAllForHomePage();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $funds,
            $request->query->get('page', 1)
        );

        $search = $this->createForm(new SearchType());

        return $this->render('AppBundle:Home:index.html.twig', array('pagination' => $pagination, 'form' => $search->createView(),));
    }

    /**
     * @param Fund $fund
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Fund $fund, $tab)
    {
        $data = $this->checkTab($tab, $fund);
        $basket = $this->checkUserBasket($this->getUser(), $fund);

        return $this->render('AppBundle:Home:show.html.twig', array(
            'fund' => $fund,
            'tab' => $tab,
            'data' => $data,
            'basket' => $basket,
        ));
    }

    /**
     * @param $tab
     * @param $fund
     * @return array
     */
    public function checkTab($tab, $fund)
    {
        $res = array();
        if ('performance' === $tab) {
            $data = $fund->getPerformance()->toArray();

            foreach ($data as $key => $obj) {
                $row = array();
                $chartData = unserialize($obj->getData());

                $series = array(
                    array(
                        'name' => $chartData['first'][0],
                        'data' =>  $chartData['first'][1],
                    ),
                    array(
                        'name' => $chartData['second'][0],
                        'data' =>  $chartData['second'][1],
                    )
                );

                $ob = new Highchart();
                $ob->chart->renderTo('linechart'.$key);
                $ob->chart->type('column');
                $ob->title->text($chartData['name']);
                $ob->xAxis->categories($chartData['categories']);

                $ob->series($series);

                $row['chart'] = $ob;
                $row['name'] = $chartData['name'];

                $res[] = $row;
            }
        }
        if ('dividends' === $tab) {
            $data = $fund->getDividends()->toArray();

            foreach($data as $obj) {
                $row = array();

                $row['type'] = $obj->getType();
                $row['amount'] = $obj->getAmount();
                $row['tax'] = $obj->getTax();
                $row['dividendDate'] = $obj->getDividendDate();
                $row['paymentDate'] = $obj->getPaymentDate();

                $res[] = $row;
            }
        }

        if ('mirrors' === $tab) {
            $data = $fund->getMirrors()->toArray();

            foreach($data as $obj) {
                $row = array();

                $row['type'] = $obj->getType();
                $row['name'] = $obj->getName();
                $row['annualCharge'] = $obj->getAnnualCharge();
                $row['dividendDate'] = $obj->getFundSize();

                $res[$row['type']][] = $row;
            }
        }

        if ('breakdown' === $tab) {
            $data = $fund->getBreakdown()->toArray();

            foreach($data as $obj) {
                $row = array();

                $name = $obj->getName();
                $percentage = (float)$obj->getPercentage();
                $type = $obj->getType();

                $res[$type][] = array($name , $percentage);
            }

            foreach ($res as $key => &$dataType) {

                $ob = new Highchart();
                $ob->chart->renderTo('linechart_'.$key);
                $ob->title->text(ucfirst($key));
                $ob->plotOptions->pie(array(
                    'allowPointSelect'  => true,
                    'cursor'    => 'pointer',
                    'dataLabels'    => array('enabled' => false),
                    'showInLegend'  => true
                ));

                $ob->series(array(array('type' => 'pie','name' => ucfirst($key), 'data' => $dataType)));

                $res[$key]['chart'] = $ob;
            }

        }

        return $res;
    }

    public function searchAction(Request $request)
    {
        $data = $request->request->get('search');
        $search = $this->createForm(new SearchType());
        $paginator = $this->get('knp_paginator');

        $res = $this->getDoctrine()->getRepository('AppBundle:Fund')->searchBy($data['search']);

        $pagination = $paginator->paginate(
            $res,
            $request->query->get('page', 1)
        );


        return $this->render('AppBundle:Home:search.html.twig', array('pagination' => $pagination, 'form' => $search->createView(),));
    }

    /**
     * @param $user
     * @param $fund
     * @return string
     */
    public function checkUserBasket($user, $fund)
    {
        if ( !is_null($user) && in_array($fund, $user->getFunds()->toArray())) {
            return 'delete';
        }

        return 'add';
    }

    public function ajaxSearchAction(Request $request)
    {
        $value = $request->get('term');
        $by = $request->get('by');

        $res = '';

        $result = $this->getDoctrine()->getRepository('AppBundle:Fund')->searchBy($value, $by)->getQuery()->getResult();

        if ($result) {
            foreach ($result as $fund) {
                $res[] = $fund->getCode();
            }
        }
        $response = new JsonResponse($res);

        return $response;
    }
}
