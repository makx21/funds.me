<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Fund;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @param Fund $fund
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFundAction(Fund $fund)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $user = $this->getUser();
            $user->addFund($fund);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'Fund was successfully add',
                'status' => 'success',
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->redirectToRoute('app_home_show_fund', array('id' => $fund->getId()));
    }

    /**
     * @param Fund $fund
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteFundAction(Fund $fund)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $user = $this->getUser();
            $user->removeFund($fund);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('funds-notice', array(
                'message' => 'Fund was successfully removed from basket',
                'status' => 'success',
            ));
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->redirectToRoute('app_home_show_fund', array('id' => $fund->getId()));
    }


    public function fundsAction(Request $request)
    {
        $funds = array();
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $user = $this->getUser();
            $funds = $user->getFunds();

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $funds,
                $request->query->get('page', 1)
            );
        } else {
            return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->render('AppBundle:Profile:funds.html.twig', array(
            'pagination' => $pagination,
        ));

    }

}