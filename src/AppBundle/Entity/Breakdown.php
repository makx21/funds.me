<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BreakdownRepository")
 * @ORM\Table(name="breakdown")
 */
class Breakdown
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Fund", inversedBy="breakdown")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $fund;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(name="rank", type="integer")
     */
    protected $rank;

    /**
     * @ORM\Column(name="prev_rank", type="integer")
     */
    protected $previousRank;

    /**
     * @ORM\Column(name="percentage", type="decimal", precision=5, scale=2)
     */
    protected $percentage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Breakdown
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Breakdown
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Breakdown
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set previousRank
     *
     * @param integer $previousRank
     * @return Breakdown
     */
    public function setPreviousRank($previousRank)
    {
        $this->previousRank = $previousRank;

        return $this;
    }

    /**
     * Get previousRank
     *
     * @return integer 
     */
    public function getPreviousRank()
    {
        return $this->previousRank;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     * @return Breakdown
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set fund
     *
     * @param \AppBundle\Entity\Fund $fund
     * @return Breakdown
     */
    public function setFund(\AppBundle\Entity\Fund $fund = null)
    {
        $this->fund = $fund;

        return $this;
    }

    /**
     * Get fund
     *
     * @return \AppBundle\Entity\Fund 
     */
    public function getFund()
    {
        return $this->fund;
    }
}
