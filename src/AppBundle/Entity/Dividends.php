<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\DividendsRepository")
 * @ORM\Table(name="dividends")
 */
class Dividends
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Fund", inversedBy="dividends")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $fund;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(name="amount", type="string", length=255)
     */
    protected $amount;

    /**
     * @ORM\Column(name="tax", type="string", length=255)
     */
    protected $tax;

    /**
     * @ORM\Column(type="date", name="dividend_date")
     */
    protected $dividendDate;

    /**
     * @ORM\Column(type="date", name="payment_date")
     */
    protected $paymentDate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Dividends
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Dividends
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set tax
     *
     * @param string $tax
     * @return Dividends
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return string 
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set dividendDate
     *
     * @param \DateTime $dividendDate
     * @return Dividends
     */
    public function setDividendDate($dividendDate)
    {
        $this->dividendDate = $dividendDate;

        return $this;
    }

    /**
     * Get dividendDate
     *
     * @return \DateTime 
     */
    public function getDividendDate()
    {
        return $this->dividendDate;
    }

    /**
     * Set paymentDate
     *
     * @param \DateTime $paymentDate
     * @return Dividends
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime 
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * Set fund
     *
     * @param \AppBundle\Entity\Fund $fund
     * @return Dividends
     */
    public function setFund(\AppBundle\Entity\Fund $fund = null)
    {
        $this->fund = $fund;

        return $this;
    }

    /**
     * Get fund
     *
     * @return \AppBundle\Entity\Fund 
     */
    public function getFund()
    {
        return $this->fund;
    }
}
