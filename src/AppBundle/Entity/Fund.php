<?php

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;


/**
 * @ORM\Entity
 * @UniqueEntity("code")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\FundsRepository")
 * @ORM\Table(name="funds")
 */
class Fund
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    protected $code;

    /**
     * @ORM\Column(type="boolean", name="parsed", options={"default": false})
     */
    protected $parsed = 0;

    /**
     * @ORM\Column(type="boolean", name="our", options={"default": false})
     */
    protected $our = 0;

    /**
     * @ORM\Column(type="boolean", name="error", options={"default": false})
     */
    protected $error = 0;

    /**
     * @ORM\OneToOne(targetEntity="Content")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     **/
    protected $content;

    /**
     * @ORM\OneToOne(targetEntity="Prices")
     * @ORM\JoinColumn(name="prices_id", referencedColumnName="id")
     **/
    protected $prices;

    /**
     * @ORM\OneToOne(targetEntity="Investment")
     * @ORM\JoinColumn(name="investment_id", referencedColumnName="id")
     **/
    protected $investment;

    /**
     * @ORM\OneToOne(targetEntity="Information")
     * @ORM\JoinColumn(name="info_id", referencedColumnName="id")
     **/
    protected $information;

    /**
     * @ORM\OneToMany(targetEntity="Performance", mappedBy="fund")
     */
    protected $performance;

    /**
     * @ORM\OneToMany(targetEntity="Dividends", mappedBy="fund")
     */
    protected $dividends;

    /**
     * @ORM\OneToMany(targetEntity="Breakdown", mappedBy="fund")
     */
    protected $breakdown;

    /**
     * @ORM\OneToMany(targetEntity="Mirrors", mappedBy="fund")
     */
    protected $mirrors;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="funds")
     **/
    protected $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->performance = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dividends = new \Doctrine\Common\Collections\ArrayCollection();
        $this->breakdown = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mirrors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Fund
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set parsed
     *
     * @param boolean $parsed
     * @return Fund
     */
    public function setParsed($parsed)
    {
        $this->parsed = $parsed;

        return $this;
    }

    /**
     * Get parsed
     *
     * @return boolean 
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    /**
     * Set content
     *
     * @param \AppBundle\Entity\Content $content
     * @return Fund
     */
    public function setContent(\AppBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \AppBundle\Entity\Content 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set prices
     *
     * @param \AppBundle\Entity\Prices $prices
     * @return Fund
     */
    public function setPrices(\AppBundle\Entity\Prices $prices = null)
    {
        $this->prices = $prices;

        return $this;
    }

    /**
     * Get prices
     *
     * @return \AppBundle\Entity\Prices 
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set investment
     *
     * @param \AppBundle\Entity\Investment $investment
     * @return Fund
     */
    public function setInvestment(\AppBundle\Entity\Investment $investment = null)
    {
        $this->investment = $investment;

        return $this;
    }

    /**
     * Get investment
     *
     * @return \AppBundle\Entity\Investment 
     */
    public function getInvestment()
    {
        return $this->investment;
    }

    /**
     * Set information
     *
     * @param \AppBundle\Entity\Information $information
     * @return Fund
     */
    public function setInformation(\AppBundle\Entity\Information $information = null)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return \AppBundle\Entity\Information 
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * Add performance
     *
     * @param \AppBundle\Entity\Performance $performance
     * @return Fund
     */
    public function addPerformance(\AppBundle\Entity\Performance $performance)
    {
        $this->performance[] = $performance;

        return $this;
    }

    /**
     * Remove performance
     *
     * @param \AppBundle\Entity\Performance $performance
     */
    public function removePerformance(\AppBundle\Entity\Performance $performance)
    {
        $this->performance->removeElement($performance);
    }

    /**
     * Get performance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPerformance()
    {
        return $this->performance;
    }

    /**
     * Add dividends
     *
     * @param \AppBundle\Entity\Dividends $dividends
     * @return Fund
     */
    public function addDividend(\AppBundle\Entity\Dividends $dividends)
    {
        $this->dividends[] = $dividends;

        return $this;
    }

    /**
     * Remove dividends
     *
     * @param \AppBundle\Entity\Dividends $dividends
     */
    public function removeDividend(\AppBundle\Entity\Dividends $dividends)
    {
        $this->dividends->removeElement($dividends);
    }

    /**
     * Get dividends
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDividends()
    {
        return $this->dividends;
    }

    /**
     * Add breakdown
     *
     * @param \AppBundle\Entity\Breakdown $breakdown
     * @return Fund
     */
    public function addBreakdown(\AppBundle\Entity\Breakdown $breakdown)
    {
        $this->breakdown[] = $breakdown;

        return $this;
    }

    /**
     * Remove breakdown
     *
     * @param \AppBundle\Entity\Breakdown $breakdown
     */
    public function removeBreakdown(\AppBundle\Entity\Breakdown $breakdown)
    {
        $this->breakdown->removeElement($breakdown);
    }

    /**
     * Get breakdown
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBreakdown()
    {
        return $this->breakdown;
    }

    /**
     * Add mirrors
     *
     * @param \AppBundle\Entity\Mirrors $mirrors
     * @return Fund
     */
    public function addMirror(\AppBundle\Entity\Mirrors $mirrors)
    {
        $this->mirrors[] = $mirrors;

        return $this;
    }

    /**
     * Remove mirrors
     *
     * @param \AppBundle\Entity\Mirrors $mirrors
     */
    public function removeMirror(\AppBundle\Entity\Mirrors $mirrors)
    {
        $this->mirrors->removeElement($mirrors);
    }

    /**
     * Get mirrors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMirrors()
    {
        return $this->mirrors;
    }

    /**
     * Add users
     *
     * @param \AppBundle\Entity\User $users
     * @return Fund
     */
    public function addUser(\AppBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \AppBundle\Entity\User $users
     */
    public function removeUser(\AppBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set our
     *
     * @param boolean $our
     * @return Fund
     */
    public function setOur($our)
    {
        $this->our = $our;

        return $this;
    }

    /**
     * Get our
     *
     * @return boolean 
     */
    public function getOur()
    {
        return $this->our;
    }

    /**
     * Set error
     *
     * @param boolean $error
     * @return Fund
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return boolean 
     */
    public function getError()
    {
        return $this->error;
    }
}
