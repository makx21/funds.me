<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\InformationRepository")
 * @ORM\Table(name="information")
 */
class Information
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="legal_struct", type="string", length=255)
     */
    protected $legalStructure;

    /**
     * @ORM\Column(type="boolean", name="isa_own", options={"default": false})
     */
    protected $isaOwn = 0;

    /**
     * @ORM\Column(name="trustee", type="string", length=255)
     */
    protected $trustee;

    /**
     * @ORM\Column(name="registrar", type="string", length=255)
     */
    protected $registrar;

    /**
     * @ORM\Column(type="boolean", name="m_manager", options={"default": false})
     */
    protected $multiManager = 0;

    /**
     * @ORM\Column(type="text", name="dividend_policy")
     */
    protected $dividendPolicy;

    /**
     * @ORM\Column(type="text", name="fund_size")
     */
    protected $fundSize;

    /**
     * @ORM\Column(type="date", name="unit_launch")
     */
    protected $unitLaunch;

    /**
     * @ORM\Column(type="date", name="fund_launch")
     */
    protected $fundLaunch;

    /**
     * @ORM\Column(type="boolean", name="save_plan", options={"default": false})
     */
    protected $savingsPlan = 0;

    /**
     * @ORM\Column(name="pricing_time", type="string", length=255)
     */
    protected $pricingTimes;

    /**
     * @ORM\Column(name="dealing_times", type="string", length=255)
     */
    protected $dealingTimes;

    /**
     * @ORM\Column(name="dealing_frequency", type="string", length=255)
     */
    protected $dealingFrequency;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legalStructure
     *
     * @param string $legalStructure
     * @return Information
     */
    public function setLegalStructure($legalStructure)
    {
        $this->legalStructure = $legalStructure;

        return $this;
    }

    /**
     * Get legalStructure
     *
     * @return string 
     */
    public function getLegalStructure()
    {
        return $this->legalStructure;
    }

    /**
     * Set isaOwn
     *
     * @param boolean $isaOwn
     * @return Information
     */
    public function setIsaOwn($isaOwn)
    {
        $this->isaOwn = $isaOwn;

        return $this;
    }

    /**
     * Get isaOwn
     *
     * @return boolean 
     */
    public function getIsaOwn()
    {
        return $this->isaOwn;
    }

    /**
     * Set trustee
     *
     * @param string $trustee
     * @return Information
     */
    public function setTrustee($trustee)
    {
        $this->trustee = $trustee;

        return $this;
    }

    /**
     * Get trustee
     *
     * @return string 
     */
    public function getTrustee()
    {
        return $this->trustee;
    }

    /**
     * Set registrar
     *
     * @param string $registrar
     * @return Information
     */
    public function setRegistrar($registrar)
    {
        $this->registrar = $registrar;

        return $this;
    }

    /**
     * Get registrar
     *
     * @return string 
     */
    public function getRegistrar()
    {
        return $this->registrar;
    }

    /**
     * Set multiManager
     *
     * @param boolean $multiManager
     * @return Information
     */
    public function setMultiManager($multiManager)
    {
        $this->multiManager = $multiManager;

        return $this;
    }

    /**
     * Get multiManager
     *
     * @return boolean 
     */
    public function getMultiManager()
    {
        return $this->multiManager;
    }

    /**
     * Set dividendPolicy
     *
     * @param string $dividendPolicy
     * @return Information
     */
    public function setDividendPolicy($dividendPolicy)
    {
        $this->dividendPolicy = $dividendPolicy;

        return $this;
    }

    /**
     * Get dividendPolicy
     *
     * @return string 
     */
    public function getDividendPolicy()
    {
        return $this->dividendPolicy;
    }

    /**
     * Set fundSize
     *
     * @param string $fundSize
     * @return Information
     */
    public function setFundSize($fundSize)
    {
        $this->fundSize = $fundSize;

        return $this;
    }

    /**
     * Get fundSize
     *
     * @return string 
     */
    public function getFundSize()
    {
        return $this->fundSize;
    }

    /**
     * Set unitLaunch
     *
     * @param \DateTime $unitLaunch
     * @return Information
     */
    public function setUnitLaunch($unitLaunch)
    {
        $this->unitLaunch = $unitLaunch;

        return $this;
    }

    /**
     * Get unitLaunch
     *
     * @return \DateTime 
     */
    public function getUnitLaunch()
    {
        return $this->unitLaunch;
    }

    /**
     * Set fundLaunch
     *
     * @param \DateTime $fundLaunch
     * @return Information
     */
    public function setFundLaunch($fundLaunch)
    {
        $this->fundLaunch = $fundLaunch;

        return $this;
    }

    /**
     * Get fundLaunch
     *
     * @return \DateTime 
     */
    public function getFundLaunch()
    {
        return $this->fundLaunch;
    }

    /**
     * Set savingsPlan
     *
     * @param boolean $savingsPlan
     * @return Information
     */
    public function setSavingsPlan($savingsPlan)
    {
        $this->savingsPlan = $savingsPlan;

        return $this;
    }

    /**
     * Get savingsPlan
     *
     * @return boolean 
     */
    public function getSavingsPlan()
    {
        return $this->savingsPlan;
    }

    /**
     * Set pricingTimes
     *
     * @param string $pricingTimes
     * @return Information
     */
    public function setPricingTimes($pricingTimes)
    {
        $this->pricingTimes = $pricingTimes;

        return $this;
    }

    /**
     * Get pricingTimes
     *
     * @return string 
     */
    public function getPricingTimes()
    {
        return $this->pricingTimes;
    }

    /**
     * Set dealingTimes
     *
     * @param string $dealingTimes
     * @return Information
     */
    public function setDealingTimes($dealingTimes)
    {
        $this->dealingTimes = $dealingTimes;

        return $this;
    }

    /**
     * Get dealingTimes
     *
     * @return string 
     */
    public function getDealingTimes()
    {
        return $this->dealingTimes;
    }

    /**
     * Set dealingFrequency
     *
     * @param string $dealingFrequency
     * @return Information
     */
    public function setDealingFrequency($dealingFrequency)
    {
        $this->dealingFrequency = $dealingFrequency;

        return $this;
    }

    /**
     * Get dealingFrequency
     *
     * @return string 
     */
    public function getDealingFrequency()
    {
        return $this->dealingFrequency;
    }
}
