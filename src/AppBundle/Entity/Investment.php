<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\InvestmentRepository")
 * @ORM\Table(name="investments")
 */
class Investment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="c_initial", type="decimal", precision=4, scale=2)
     */
    protected $c_initial;

    /**
     * @ORM\Column(name="annual", type="decimal", precision=4, scale=2)
     */
    protected $annual;

    /**
     * @ORM\Column(name="ocf", type="decimal", precision=4, scale=2)
     */
    protected $ocf;

    /**
     * @ORM\Column(name="m_initial", type="integer")
     */
    protected $m_initial;

    /**
     * @ORM\Column(name="additional", type="integer")
     */
    protected $additional;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set c_initial
     *
     * @param string $cInitial
     * @return Investment
     */
    public function setCInitial($cInitial)
    {
        $this->c_initial = $cInitial;

        return $this;
    }

    /**
     * Get c_initial
     *
     * @return string 
     */
    public function getCInitial()
    {
        return $this->c_initial;
    }

    /**
     * Set annual
     *
     * @param string $annual
     * @return Investment
     */
    public function setAnnual($annual)
    {
        $this->annual = $annual;

        return $this;
    }

    /**
     * Get annual
     *
     * @return string 
     */
    public function getAnnual()
    {
        return $this->annual;
    }

    /**
     * Set ocf
     *
     * @param string $ocf
     * @return Investment
     */
    public function setOcf($ocf)
    {
        $this->ocf = $ocf;

        return $this;
    }

    /**
     * Get ocf
     *
     * @return string 
     */
    public function getOcf()
    {
        return $this->ocf;
    }

    /**
     * Set m_initial
     *
     * @param integer $mInitial
     * @return Investment
     */
    public function setMInitial($mInitial)
    {
        $this->m_initial = $mInitial;

        return $this;
    }

    /**
     * Get m_initial
     *
     * @return integer 
     */
    public function getMInitial()
    {
        return $this->m_initial;
    }

    /**
     * Set additional
     *
     * @param integer $additional
     * @return Investment
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get additional
     *
     * @return integer 
     */
    public function getAdditional()
    {
        return $this->additional;
    }
}
