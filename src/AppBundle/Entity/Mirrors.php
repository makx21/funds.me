<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MirrorsRepository")
 * @ORM\Table(name="mirrors")
 */
class Mirrors
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Fund", inversedBy="mirrors")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $fund;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(name="annual_charge", type="decimal", precision=8, scale=2)
     */
    protected $annualCharge;

    /**
     * @ORM\Column(name="fund_size", type="decimal", precision=8, scale=2)
     */
    protected $fundSize;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Mirrors
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mirrors
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set annualCharge
     *
     * @param string $annualCharge
     * @return Mirrors
     */
    public function setAnnualCharge($annualCharge)
    {
        $this->annualCharge = $annualCharge;

        return $this;
    }

    /**
     * Get annualCharge
     *
     * @return string 
     */
    public function getAnnualCharge()
    {
        return $this->annualCharge;
    }

    /**
     * Set fundSize
     *
     * @param string $fundSize
     * @return Mirrors
     */
    public function setFundSize($fundSize)
    {
        $this->fundSize = $fundSize;

        return $this;
    }

    /**
     * Get fundSize
     *
     * @return string 
     */
    public function getFundSize()
    {
        return $this->fundSize;
    }

    /**
     * Set fund
     *
     * @param \AppBundle\Entity\Fund $fund
     * @return Mirrors
     */
    public function setFund(\AppBundle\Entity\Fund $fund = null)
    {
        $this->fund = $fund;

        return $this;
    }

    /**
     * Get fund
     *
     * @return \AppBundle\Entity\Fund 
     */
    public function getFund()
    {
        return $this->fund;
    }
}
