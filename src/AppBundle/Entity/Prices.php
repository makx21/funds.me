<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PricesRepository")
 * @ORM\Table(name="prices")
 */
class Prices
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    protected $type;

    /**
     * @ORM\Column(name="currency", type="string", length=10)
     */
    protected $currency;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    protected $offer;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(name="yield", type="decimal", precision=4, scale=2)
     */
    protected $yield;

    /**
     * @ORM\Column(name="citicode", type="string", length=10)
     */
    protected $citicode;

    /**
     * @ORM\Column(name="cedol", type="string", length=15)
     */
    protected $cedol;

    /**
     * @ORM\Column(name="icin", type="string", length=255)
     */
    protected $icin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Prices
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Prices
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set offer
     *
     * @param string $offer
     * @return Prices
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return string 
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Prices
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set yield
     *
     * @param string $yield
     * @return Prices
     */
    public function setYield($yield)
    {
        $this->yield = $yield;

        return $this;
    }

    /**
     * Get yield
     *
     * @return string 
     */
    public function getYield()
    {
        return $this->yield;
    }

    /**
     * Set citicode
     *
     * @param string $citicode
     * @return Prices
     */
    public function setCiticode($citicode)
    {
        $this->citicode = $citicode;

        return $this;
    }

    /**
     * Get citicode
     *
     * @return string 
     */
    public function getCiticode()
    {
        return $this->citicode;
    }

    /**
     * Set cedol
     *
     * @param string $cedol
     * @return Prices
     */
    public function setCedol($cedol)
    {
        $this->cedol = $cedol;

        return $this;
    }

    /**
     * Get cedol
     *
     * @return string 
     */
    public function getCedol()
    {
        return $this->cedol;
    }

    /**
     * Set icin
     *
     * @param string $icin
     * @return Prices
     */
    public function setIcin($icin)
    {
        $this->icin = $icin;

        return $this;
    }

    /**
     * Get icin
     *
     * @return string 
     */
    public function getIcin()
    {
        return $this->icin;
    }
}
