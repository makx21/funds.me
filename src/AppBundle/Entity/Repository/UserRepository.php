<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAll()
    {
        $qb =  $this->createQueryBuilder('u');

        $qb->addOrderBy('u.createdAt', 'ASC');

        return $qb;
    }

    /**
     * Return country code
     * @param int $id
     * @return mixed
     */
    public function getUserCountry($id)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u.country');
        $qb->where('u.id = :id');
        $qb->setParameter('id', $id, \Doctrine\DBAL\Types\Type::INTEGER);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
