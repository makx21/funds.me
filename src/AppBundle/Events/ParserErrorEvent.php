<?php

namespace AppBundle\Events;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class ParserErrorEvent
 * @package AppBundle\Events
 */
class ParserErrorEvent extends Event
{
    protected $message;

    /**
     * @param string $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}