<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Title',
            'required' => false,
        ))
            ->add('objective', 'textarea', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Objective ',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'content';
    }
}
