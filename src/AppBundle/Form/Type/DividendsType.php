<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class DividendsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Dividend type',
            'required' => false,
        ))
            ->add('amount', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Dividend amount ',
                'required' => false,
            ))
            ->add('tax', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Tax indicator ',
                'required' => false,
            ))
            ->add('dividendDate', 'date', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Ex dividend date ',
                'required' => false,
            ))
            ->add('paymentDate', 'date', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Payment date ',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dividends',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dividends';
    }
}
