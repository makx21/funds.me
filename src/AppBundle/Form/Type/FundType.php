<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FundType
 * @package AppBundle\Type\Form
 */
class FundType extends AbstractType
{
    protected $withContent;

    /**
     * @param bool $withContent
     */
    public function __construct($withContent = false)
    {
        $this->withContent = $withContent;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', 'text', array(
            'label' => 'Paste Code: ',
            'attr' => array('style' => 'width: 800px; margin: 10px', 'class' => 'form-control'),
            'error_bubbling' => true,
        ));

        if ($this->withContent) {
            $builder->add('content', new ContentType(), array(
                'label' => ' ',
            ));
            $builder->add('prices', new PricesType(), array('label' => ' ' ));
            $builder->add('investment', new InvestmentType(), array('label' => ' ' ));
            $builder->add('information', new InformationType(), array('label' => ' ' ));
            $builder->add('dividends', 'collection', array(
                'type'=> new DividendsType(),
                'allow_add'=>true,
                'by_reference'=>false
            ));
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'fund';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true
        ));
    }
}