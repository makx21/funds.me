<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class InformationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('legalStructure', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Legal Structure',
            'required' => false,
        ))
            ->add('isaOwn', 'checkbox', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Own ISA wrapper ',
                'required' => false,
            ))
            ->add('trustee', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Trustee / Depositary ',
                'required' => false,
            ))
            ->add('registrar', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Registrar ',
                'required' => false,
            ))
            ->add('multiManager', 'checkbox', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Multi-Manager ',
                'required' => false,
            ))
            ->add('dividendPolicy', 'textarea', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Dividend policy ',
                'required' => false,
            ))
            ->add('fundSize', 'textarea', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Fund size ',
                'required' => false,
            ))
            ->add('unitLaunch', 'date', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Unit Launch ',
                'required' => false,
            ))
            ->add('fundLaunch', 'date', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Fund Launch ',
                'required' => false,
            ))
            ->add('savingsPlan', 'checkbox', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Savings plan ',
                'required' => false,
            ))
            ->add('pricingTimes', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Pricing times ',
                'required' => false,
            ))
            ->add('dealingTimes', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Dealing times',
                'required' => false,
            ))
            ->add('dealingFrequency', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Dealing frequency',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Information',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'information';
    }
}
