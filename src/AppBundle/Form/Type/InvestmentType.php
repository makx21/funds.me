<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class InvestmentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('c_initial', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Chargers initial',
            'required' => false,
        ))
            ->add('annual', 'number', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Chargers annual ',
                'required' => false,
            ))
            ->add('ocf', 'number', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Chargers ocf ',
                'required' => false,
            ))
            ->add('m_initial', 'number', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Minimum Investment Initial ',
                'required' => false,
            ))
            ->add('additional', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Minimum Investment Additional ',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Investment',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'investment';
    }
}
