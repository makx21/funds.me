<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class PricesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Type',
            'required' => false,
        ))
            ->add('currency', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Currency ',
                'required' => false,
            ))
            ->add('offer', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Offer ',
                'required' => false,
            ))
            ->add('date', 'date', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Date ',
                'required' => false,
            ))
            ->add('yield', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Yield ',
                'required' => false,
            ))
            ->add('citicode', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'Citicode ',
                'required' => false,
            ))
            ->add('cedol', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'SEDOL ',
                'required' => false,
            ))
            ->add('icin', 'text', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
                'label' => 'ISIN ',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Prices',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'prices';
    }
}
