<?php

namespace AppBundle;

/**
 * Contains all events thrown in our Funds application
 */
final class FundsEvents
{
    /**
     * The PARSER_ERROR event occurs when parser finished with errors.
     *
     * The event listener receives a AppBundle\EventListeners\ParserErrorListener instance.
     */
    const PARSER_ERROR = 'parser.error';
}
