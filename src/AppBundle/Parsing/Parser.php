<?php

namespace AppBundle\Parsing;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Parsing\Graber;

/**
 * Class Parser
 * @package AppBundle\Parsing
 */
class Parser
{
    const PARSER_URL = 'http://www.trustnet.com/Factsheets/Factsheet.aspx?univ=O&fundCode=';

    const RATING_URL = 'http://www.trustnet.com/ratings/?univ=O&moreresult=true';

    const RATING_PAGE = '&Fr_PageNo=';

    const PARSER_URL_TAB_PERFORMANCE = '&pagetype=performance';

    const PARSER_URL_TAB_DIVIDENDS = '&pagetype=dividends';

    const PARSER_URL_TAB_BREAKDOWN = '&pagetype=portfoliobreakdown';

    const PARSER_URL_TAB_MIRRORS = '&pagetype=life-pension-mirrors';

    /**
     * @var string $code;;
     */
    protected $code;

    /**
     * Main url with fund code
     *
     * @var string $url
     */
    protected $url;

    /**
     * @var Graber
     */
    protected $graber;

    /**
     * @param Graber $graber
     */
    public function __construct(Graber $graber)
    {
        $this->graber = $graber;
    }

    /**
     * @param $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        $this->url = self::PARSER_URL.$this->getCode();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Do main parse by tabs
     *
     * @return array
     */
    public function doParse()
    {
        $results = array();

        if (!$this->checkCode()) {

            if (!$this->checkFundType()) {
                $results[] = $this->parseFundCenter();
                $results[] = $this->parsePerformance();
                $results[] = $this->parseDividends();
                $results[] = $this->parseBreakdown();
                $results[] = $this->parseMirrors();
            } else {
                // Non-subscribing fund
                $results[] = $this->parseNonOverview();
                $results[] = $this->parseNonPerformance();

                $results['type'] = 'none';
            }
        }

        return $results;
    }


    public function updateFundsRating()
    {
        $list = array();

        for ($pageNumber = 1; $pageNumber <= 7; $pageNumber++) {
            $content = $this->graber->getData(Parser::RATING_URL.Parser::RATING_PAGE.$pageNumber)->getBody(true);

            $parser = new Crawler($content);

            $countCodesInCurrentPage = $parser->filter('#fundslist thead tr')->count();

            for ($i = 2; $i <= $countCodesInCurrentPage; $i++) {
                $code = trim($parser->filter("#fundslist thead tr:nth-child({$i}) td:nth-child(4)")->html());

                $code = substr($code, strpos($code, 'fundCode') + 9);
                $code = substr($code, 0, strpos($code, '&'));

                $list[] = $code;
            }
        }

        return $list;

    }

    public function checkFundType()
    {
        $content = $this->graber->getData($this->url)->getBody(true);
        $parser = new Crawler($content);

        return $parser->filter('html:contains("Non-subscribing fund")')->count() ? true : false;
    }

    public function parseNonOverview()
    {
        $content = $this->graber->getData($this->url)->getBody(true);

        $parser = new Crawler($content);
        $results = array();
        $fundName = '';

        try {
            // Prices
            $prices = array();
            try {
                $fundName = $parser->filter('.fundName h1')->html();

                for ($i=2; $i <= 10 ; $i++) {
                    if (in_array($i, array(3))) {
                        continue;
                    }

                    $prices[] = trim($parser->filter("caption + tbody > .altrow td:nth-child({$i})")->html());
                }
            } catch (\Exception $e) {
            }

        } catch (\Exception $e) {
            $prices = '';
        }

        if ('' === $prices[1]){
            unset($prices[1]);
            $prices = array_values($prices);

            array_splice( $prices, 2, 0, array('') );
            unset($prices[5]);
            $prices = array_values($prices);

        } else {
            $prices[5] = $prices[6];
            $prices[6] = $prices[7];
        }

        $currency = trim(substr($prices[1], strpos($prices[1],' ')));
        $offer = trim(substr($prices[1], 0, strpos($prices[1],' ')));

        $prices[1] = $currency;
        $prices[2] = $offer;

        $prices[4] = str_replace('n/a', '0', $prices[4]);
        $prices[7] = '';


        $results['fund']['prices'] = $prices;
        $results['fund']['name'] = preg_replace("/&#?[a-z0-9]+;/i","", strip_tags(trim($fundName)));

        return $results;
    }

    public function parseNonPerformance()
    {
        $content = $this->graber->getData($this->url.Parser::PARSER_URL_TAB_PERFORMANCE)->getBody(true);

        $parser = new Crawler($content);
        $results = array();

        try {
            $categoriesCount = $parser->filter('.altrow td')->count();

            for ($i = 2; $i <= $categoriesCount; $i++) {
                $categories[] = trim($parser->filter(".format:nth-of-type(1) tr th:nth-child({$i})")->text());
            }

            $name = $parser->filter('#cumu h1')->html();

            $fundNameFirst = trim($parser
                ->filter('.altrow td:nth-child(1)')->html());


            for ($i = 2; $i <= $categoriesCount; $i++) {
                $fundFirstData[] = (float)trim($parser
                    ->filter(".altrow td:nth-child({$i})")->text());
            }

            $fundNameSecond = trim($parser
                ->filter('.odd td:nth-child(1)')->html());


            for ($i = 2; $i <= $categoriesCount; $i++) {
                $fundSecondData[] = (float)trim($parser
                    ->filter(".odd td:nth-child({$i})")->text());
            }

        } catch (\Exception $e) {
            $name = '';
            $categories = array();
            $fundNameFirst = '';
            $fundFirstData = array();
            $fundNameSecond = '';
            $fundSecondData = array();
        }

        $results['performance']['quarter-end']['name'] = strip_tags($name);
        $results['performance']['quarter-end']['categories'] = $categories;
        $results['performance']['quarter-end']['first'] = array(strip_tags($fundNameFirst), $fundFirstData);
        $results['performance']['quarter-end']['second'] = array(strip_tags($fundNameSecond), $fundSecondData);

        return $results;
    }

    /**
     * Parse Mirrors tab
     *
     * @return array
     */
    public function parseMirrors()
    {
        $content = $this->graber->getData($this->url.self::PARSER_URL_TAB_MIRRORS)->getBody(true);
        $parser = new Crawler($content);
        $results = array();

        // Find Mirrors data
        try {
            // Life fund
            $lifeData = array();

            $count = $parser->filter(".life_pension_content:nth-child(1) table tr")->count();

            for ($i = 1; $i <= $count; $i++) {
                $rowData = array();
                for ($j=1; $j <= 3; $j++) {
                    $col = strip_tags(trim($parser->filter(".life_pension_content:nth-child(1) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    $rowData[] = $col;
                }

                $lifeData[] = $rowData;
            }
        } catch (\Exception $e) {
        }

        $results['mirrors']['life'] = $lifeData;

        try {
            // Pension fund
            $pensionData = array();

            $count = $parser->filter(".life_pension_content:nth-child(2) table tr")->count();

            for ($i = 1; $i <= $count; $i++) {
                $rowData = array();
                for ($j=1; $j <= 3; $j++) {
                    $col = strip_tags(trim($parser->filter(".life_pension_content:nth-child(2) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    $rowData[] = $col;
                }

                $pensionData[] = $rowData;
            }
        } catch (\Exception $e) {
        }

        $results['mirrors']['pension'] = $pensionData;


        return $results;
    }

    /**
     * Parse Breakdown tab
     *
     * @return array
     */
    public function parseBreakdown()
    {
        $content = $this->graber->getData($this->url.self::PARSER_URL_TAB_BREAKDOWN)->getBody(true);
        $parser = new Crawler($content);
        $results = array();

        // Find Breakdown data
        try {
            // Holdings
            $holdingsName = trim($parser->filter('.breakdowncolumnset:nth-child(1) h4')->text());

            $holdingData = array();

            for ($i = 2; $i <= 12; $i++) {
                $rowData = array();
                for ($j=2; $j <= 5; $j++) {
                    $col = strip_tags(trim($parser->filter(".breakdowncolumnset:nth-child(1) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    if ('' === $col) {
                        continue;
                    }
                    $rowData[] = $col;
                }

                $holdingData[] = $rowData;
            }
        } catch (\Exception $e) {
        }

        $results['breakdown']['holdings']['name'] = $holdingsName;
        $results['breakdown']['holdings']['data'] = array_filter($holdingData);

        try {
            // Asset
            $assetName = trim($parser->filter('.breakdowncolumnset:nth-child(2) h4')->text());

            $assetData = array();

            for ($i = 2; $i <= 12; $i++) {
                $rowData = array();
                for ($j=2; $j <= 5; $j++) {
                    $col = strip_tags(trim($parser->filter(".breakdowncolumnset:nth-child(2) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    if ('' === $col) {
                        continue;
                    }
                    $rowData[] = $col;
                }

                $assetData[] = $rowData;

            }
        } catch (\Exception $e) {
        }

        $results['breakdown']['asset']['name'] = $assetName;
        $results['breakdown']['asset']['data'] = array_filter($assetData);

        try {
            // Regions
            $regionsName = trim($parser->filter('.breakdowncolumnset:nth-child(3) h4')->text());

            $regionsData = array();

            for ($i = 2; $i <= 12; $i++) {
                $rowData = array();
                for ($j=2; $j <= 5; $j++) {
                    $col = strip_tags(trim($parser->filter(".breakdowncolumnset:nth-child(3) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    if ('' === $col) {
                        continue;
                    }
                    $rowData[] = $col;
                }

                $regionsData[] = $rowData;
            }
        } catch (\Exception $e) {
        }

        $results['breakdown']['regions']['name'] = $regionsName;
        $results['breakdown']['regions']['data'] = array_filter($regionsData);

        try {
            // Sectors
            $sectorsName = trim($parser->filter('.breakdowncolumnset:nth-child(4) h4')->text());

            $sectorsData = array();

            for ($i = 2; $i <= 12; $i++) {
                $rowData = array();
                for ($j=2; $j <= 5; $j++) {
                    $col = strip_tags(trim($parser->filter(".breakdowncolumnset:nth-child(4) table tr:nth-child({$i}) td:nth-child({$j})")->html()));
                    if ('' === $col) {
                        continue;
                    }
                    $rowData[] = $col;
                }

                $sectorsData[] = $rowData;
            }
        } catch (\Exception $e) {
        }

        $results['breakdown']['sectors']['name'] = $sectorsName;
        $results['breakdown']['sectors']['data'] = array_filter($sectorsData);

        return $results;
    }

    /**
     * Parse Dividends tab
     *
     * @return array
     */
    public function parseDividends()
    {
        $content = $this->graber->getData($this->url.self::PARSER_URL_TAB_DIVIDENDS)->getBody(true);
        $parser = new Crawler($content);
        $results = array();

        // Find Dividends data
        try {
            $dividendCount = $parser->filter('.dividend_content table tr')->count();
            $dividendData = array();

            for ($i = 2; $i <= $dividendCount; $i++) {
                $rowData = array();
                for ($j=1; $j <= 5; $j++) {
                    $rowData[] = trim($parser->filter(".dividend_content table tr:nth-child({$i}) td:nth-child({$j})")->text());
                }
                $dividendData[] = $rowData;
            }

        } catch (\Exception $e) {
            $dividendData = array();
        }

        $results['dividends'] = $dividendData;

        return $results;
    }

    /**
     * Parse Performance tab
     *
     * @return array
     */
    public function parsePerformance()
    {
        $content = $this->graber->getData($this->url.self::PARSER_URL_TAB_PERFORMANCE)->getBody(true);
        $parser = new Crawler($content);
        $results = array();

        // Find quarter-end performance
        try {
            $name = trim($parser->filter('.panel_widget:nth-child(2) h4')->text());

            $categoriesCount = $parser->filter('.panel_widget:nth-child(2) table th')->count();

            for ($i = 3; $i <= $categoriesCount; $i++) {
                $categories[] = trim($parser->filter(".panel_widget:nth-child(2) table th:nth-child({$i})")->text());
            }

            $fundNameFirst = trim($parser
                ->filter('.panel_widget:nth-child(2) table tr:nth-child(2) td:nth-child(2)')->html());

            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundFirstData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(2) table tr:nth-child(2) td:nth-child({$i})")->text());
            }

            $fundNameSecond = trim($parser
                ->filter('.panel_widget:nth-child(2) table tr:nth-child(3) td:nth-child(2)')->html());

            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundSecondData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(2) table tr:nth-child(3) td:nth-child({$i})")->text());
            }

        } catch (\Exception $e) {
            $name = '';
            $categories = array();
            $fundNameFirst = '';
            $fundFirstData = array();
            $fundNameSecond = '';
            $fundSecondData = array();
        }
        $results['performance']['quarter-end']['name'] = strip_tags($name);
        $results['performance']['quarter-end']['categories'] = $categories;
        $results['performance']['quarter-end']['first'] = array(strip_tags($fundNameFirst), $fundFirstData);
        $results['performance']['quarter-end']['second'] = array(strip_tags($fundNameSecond), $fundSecondData);


        // Find rolling performance
        try {
            $name = trim($parser->filter('.panel_widget:nth-child(3) h4')->text());

            $categoriesCount = $parser->filter('.panel_widget:nth-child(3) table th')->count();
            $categories = array();

            for ($i = 3; $i <= $categoriesCount; $i++) {
                $categories[] = trim($parser->filter(".panel_widget:nth-child(3) table th:nth-child({$i})")->text());
            }

            $fundNameFirst = trim($parser
                ->filter('.panel_widget:nth-child(3) table tr:nth-child(2) td:nth-child(2)')->html());

            $fundFirstData = array();
            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundFirstData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(3) table tr:nth-child(2) td:nth-child({$i})")->text());
            }

            $fundNameSecond = trim($parser
                ->filter('.panel_widget:nth-child(3) table tr:nth-child(3) td:nth-child(2)')->html());

            $fundSecondData = array();
            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundSecondData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(3) table tr:nth-child(3) td:nth-child({$i})")->text());
            }

        } catch (\Exception $e) {
            $name = '';
            $categories = array();
            $fundNameFirst = '';
            $fundFirstData = array();
            $fundNameSecond = '';
            $fundSecondData = array();
        }

        $results['performance']['rolling']['name'] = strip_tags($name);
        $results['performance']['rolling']['categories'] = $categories;
        $results['performance']['rolling']['first'] = array(strip_tags($fundNameFirst), $fundFirstData);
        $results['performance']['rolling']['second'] = array(strip_tags($fundNameSecond), $fundSecondData);


        // Find calendar year performance
        try {
            $name = trim($parser->filter('.panel_widget:nth-child(4) h4')->text());

            $categoriesCount = $parser->filter('.panel_widget:nth-child(4) table th')->count();
            $categories = array();

            for ($i = 3; $i <= $categoriesCount; $i++) {
                $categories[] = trim($parser->filter(".panel_widget:nth-child(4) table th:nth-child({$i})")->text());
            }

            $fundNameFirst = trim($parser
                ->filter('.panel_widget:nth-child(4) table tr:nth-child(2) td:nth-child(2)')->html());

            $fundFirstData = array();
            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundFirstData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(4) table tr:nth-child(2) td:nth-child({$i})")->text());
            }

            $fundNameSecond = trim($parser
                ->filter('.panel_widget:nth-child(4) table tr:nth-child(3) td:nth-child(2)')->html());

            $fundSecondData = array();
            for ($i = 3; $i <= $categoriesCount; $i++) {
                $fundSecondData[] = (float)trim($parser
                    ->filter(".panel_widget:nth-child(4) table tr:nth-child(3) td:nth-child({$i})")->text());
            }

        } catch (\Exception $e) {
            $name = '';
            $categories = array();
            $fundNameFirst = '';
            $fundFirstData = array();
            $fundNameSecond = '';
            $fundSecondData = array();
        }

        $results['performance']['year']['name'] = strip_tags($name);
        $results['performance']['year']['categories'] = $categories;
        $results['performance']['year']['first'] = array(strip_tags($fundNameFirst), $fundFirstData);
        $results['performance']['year']['second'] = array(strip_tags($fundNameSecond), $fundSecondData);

        return $results;
    }

    /**
     * Parse Fund Center tab
     *
     * @return array
     */
    private function parseFundCenter()
    {
        $content = $this->graber->getData($this->url)->getBody(true);

        $parser = new Crawler($content);
        $results = array();

        // Find Fund Name
        try {
            $name = $parser->filter('.fund_detail h1')->html();
        } catch (\Exception $e) {
            $name = '';
        }
        $results['fund']['name'] = preg_replace("/&#?[a-z0-9]+;/i","", strip_tags(trim($name)));

        // Find Fund’s objective
        try {
            $objective = $parser->filter('.fund_objective p')->html();
        } catch (\Exception $e) {
            $objective = '';
        }
        $results['fund']['objective'] = trim($objective);

        // Find Invest details
        try {
            // Charges
            $c_initial = trim($parser->filter('.invest_details td:nth-child(2)')->text());
            $annual = trim($parser->filter('.invest_details table tr:nth-child(3) td:nth-child(2)')->text());
            $ocf = trim($parser->filter('.invest_details table tr:nth-child(4) td:nth-child(3)')->text());
        } catch (\Exception $e) {
            $c_initial = 0;
            $annual = 0;
            $ocf = 0;
        }

        try {
            // Minimum Investment
            $m_initial = trim($parser->filter('.invest_details table + table tr:nth-child(2) td:nth-child(2)')->text());
            $additional = trim($parser->filter('.invest_details table + table tr:nth-child(3) td:nth-child(2)')->text());
        } catch (\Exception $e) {
            $m_initial = 0;
            $additional = 0;
        }

        $m_initial = str_replace('n/a', '0', $m_initial);
        $c_initial = str_replace('n/a', '0', $c_initial);
        $additional = str_replace('n/a', '0', $additional);
        $annual = str_replace('n/a', '0', $annual);
        $ocf = str_replace('n/a', '0', $ocf);

        $results['fund']['investment']['charges'] = array(rtrim($c_initial, '%'), rtrim($annual, '%'), rtrim($ocf, '%'));
        $results['fund']['investment']['minimum'] = array(str_replace(',', '', trim(mb_substr($m_initial, 1))),
            str_replace(',', '', trim(mb_substr($additional, 1))));


        // Prices
        $prices = array();
        try {
            for ($i=2; $i <= 9 ; $i++) {
                $prices[] = trim($parser->filter(".panel_widget.price table tr:nth-child(2) td:nth-child({$i})")->html());
            }
        } catch (\Exception $e) {
        }

        $prices[4] = str_replace('n/a', '0', $prices[4]);
        $prices[5] = str_replace('n/a', '0', $prices[5]);
        $prices[7] = str_replace('n/a', '0', $prices[7]);

        if (is_numeric($prices[3])) {
            $prices[2] = $prices[3];

            if ('' === $prices[2]) {
                $prices[2] = 0;
            }

            unset($prices[3]);
            $prices = array_values($prices);
        }

        $results['fund']['prices'] = $prices;

        // Additional Information
        try {
            // First table
            $legalStructure = trim($parser->filter('.additional_information table tr:nth-child(1) td:nth-child(2)')->text());
            $isa = trim($parser->filter('.additional_information table tr:nth-child(2) td:nth-child(2)')->text());
            $trustee = trim($parser->filter('.additional_information table tr:nth-child(3) td:nth-child(2)')->text());
            $registrar = trim($parser->filter('.additional_information table tr:nth-child(4) td:nth-child(2)')->text());
            $multiManager = trim($parser->filter('.additional_information table tr:nth-child(5) td:nth-child(2)')->text());
        } catch (\Exception $e) {
            $legalStructure = '';
            $isa = '';
            $trustee = '';
            $registrar = '';
            $multiManager = '';
        }

        try {
            // Second table
            $dividendPolicy = trim($parser->filter('.additional_information .three_cols:nth-child(2) tr:nth-child(1) td:nth-child(2)')->text());
            $fundSize = trim($parser->filter('.additional_information .three_cols:nth-child(2) tr:nth-child(2) td:nth-child(2)')->text());
            $unitLaunch = trim($parser->filter('.additional_information .three_cols:nth-child(2) tr:nth-child(3) td:nth-child(2)')->text());
            $fundLaunch = trim($parser->filter('.additional_information .three_cols:nth-child(2) tr:nth-child(4) td:nth-child(2)')->text());
        } catch (\Exception $e) {
            $dividendPolicy = '';
            $fundSize = '';
            $unitLaunch = '';
            $fundLaunch = '';
        }

        try {
            // Third table
            $savingsPlan = trim($parser->filter('.additional_information .three_cols:nth-child(3) tr:nth-child(1) td:nth-child(2)')->text());
            $pricingTimes = trim($parser->filter('.additional_information .three_cols:nth-child(3) tr:nth-child(2) td:nth-child(2)')->text());
            $dealingTimes = trim($parser->filter('.additional_information .three_cols:nth-child(3) tr:nth-child(3) td:nth-child(2)')->text());
            $dealingFrequency = trim($parser->filter('.additional_information .three_cols:nth-child(3) tr:nth-child(4) td:nth-child(2)')->text());
        } catch (\Exception $e) {
            $savingsPlan = '';
            $pricingTimes = '';
            $dealingTimes = '';
            $dealingFrequency = '';
        }

        $results['fund']['additionalInformation']['firstTable'] = array(
            $legalStructure,
            strtolower($isa) === 'yes' ? 1 : 0,
            $trustee,
            $registrar,
            strtolower($multiManager) === 'yes' ? 1 : 0
        );
        $results['fund']['additionalInformation']['secondTable'] = array(
            $dividendPolicy,
            $fundSize,
            $unitLaunch,
            $fundLaunch,
        );
        $results['fund']['additionalInformation']['thirdTable'] = array(
            strtolower($savingsPlan)  === 'yes' ? 1 : 0,
            $pricingTimes,
            $dealingTimes,
            $dealingFrequency,
        );

        return $results;
    }

    public function checkCode()
    {
        $content = $this->graber->getData($this->url)->getBody(true);

        $parser = new Crawler($content);

        return (
            $parser->filter('html:contains("Error message")')->count() ||
            $parser->filter('html:contains("invalid fundcode/universe")')->count())  ? true : false;
    }

}