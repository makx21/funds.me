<?php

namespace AppBundle\Parsing;

use AppBundle\Parsing\Saver\SaveParseSql;
use Doctrine\ORM\EntityManager;


/**
 * Class ParserManager
 * @package AppBundle\Parsing
 */
class ParserManager
{
    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var Saver
     */
    protected $saver;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var SaveParseSql
     */
    protected $sqlSaver;

    /**
     * @param Parser        $parser
     * @param Saver         $saver
     * @param EntityManager $em
     * @param SaveParseSql  $sqlSaver
     */
    public function __construct(Parser $parser, Saver $saver, EntityManager $em, SaveParseSql $sqlSaver)
    {
        $this->parser = $parser;
        $this->saver = $saver;
        $this->em = $em;
        $this->sqlSaver = $sqlSaver;
    }

    /**
     * Run parser manager
     * @param $all
     */
    public function run($all)
    {
        $funds = $all ?
            $this->em->getRepository('AppBundle:Fund')->findAll()->getQuery()->getResult() :
            $this->em->getRepository('AppBundle:Fund')->findOnlyNotParsedNews()->getQuery()->getResult();

        if (!empty($funds)) {
            foreach ($funds as $fund) {
                $this->parser->setCode($fund->getCode());
                $results = $this->parser->doParse();

                $this->saver->saveData($fund->getCode(), $results);
            }
        }

    }

    public function runSpecificFund($code)
    {
        $fund = $this->em->getRepository('AppBundle:Fund')->findOneByCode($code);

        if (!empty($fund)) {
            $this->parser->setCode($fund->getCode());
            $results = $this->parser->doParse();

            $this->saver->saveData($fund->getCode(), $results);
        }
    }

    public function updateRating()
    {
        $fundsCode = $this->parser->updateFundsRating();

        if (!empty($fundsCode)) {
            $this->sqlSaver->startSql('funds', 'code');

            foreach ($fundsCode as $code) {
                $this->sqlSaver->addSql(
                    array(
                        'code' => $code,
                        'created_at' => date('Y-m-d H:i:s'),
                    )
                );
            }

            $this->sqlSaver->finishSql();
        }

    }


}