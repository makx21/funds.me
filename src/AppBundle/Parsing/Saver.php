<?php

namespace AppBundle\Parsing;

use AppBundle\Entity\Breakdown;
use AppBundle\Entity\Content;
use AppBundle\Entity\Dividends;
use AppBundle\Entity\Fund;
use AppBundle\Entity\Information;
use AppBundle\Entity\Investment;
use AppBundle\Entity\Mirrors;
use AppBundle\Entity\Performance;
use AppBundle\Entity\Prices;
use Doctrine\ORM\EntityManager;

/**
 * Class Saver
 * @package AppBundle\Parsing
 */
class Saver
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $fundCode
     * @param array $data
     */
    public function saveData($fundCode, array $data)
    {
        $fund = $this->em->getRepository('AppBundle:Fund')->findOneByCode($fundCode);

        if (empty($data)) {
            $fund->setError(1);
            $this->em->persist($fund);
        } else {

            if (!$fund->getParsed()) {
                $content = new Content();
                $investment = new Investment();
                $info = new Information();
                $prices = new Prices();
            } else {
                $content = $fund->getContent();
                $investment = $fund->getInvestment();
                $info = $fund->getInformation();;
                $prices = $fund->getPrices();
            }

            $fundCenter = $data[0];

            $content->setName(isset($fundCenter['fund']['name']) ? $fundCenter['fund']['name'] : '');
            $content->setObjective(isset($fundCenter['fund']['objective']) ? $fundCenter['fund']['objective'] : 'This fund does not subscribe');

            $investment->setCInitial(isset($fundCenter['fund']['investment']['charges'][0]) ? $fundCenter['fund']['investment']['charges'][0] : 0);
            $investment->setAnnual(isset($fundCenter['fund']['investment']['charges'][1]) ? $fundCenter['fund']['investment']['charges'][1] : 0);
            $investment->setOcf(isset($fundCenter['fund']['investment']['charges'][2]) ? $fundCenter['fund']['investment']['charges'][2] : 0);
            $investment->setMInitial(isset($fundCenter['fund']['investment']['minimum'][0]) ? (int)$fundCenter['fund']['investment']['minimum'][0] : 0);
            $investment->setAdditional(isset($fundCenter['fund']['investment']['minimum'][1]) ? (int)$fundCenter['fund']['investment']['minimum'][1] : 0);

            $prices->setType(isset($fundCenter['fund']['prices'][0]) ? $fundCenter['fund']['prices'][0] : '');
            $prices->setCurrency(isset($fundCenter['fund']['prices'][1]) ? $fundCenter['fund']['prices'][1] : '');
            $prices->setOffer(isset($fundCenter['fund']['prices'][2]) ? $fundCenter['fund']['prices'][2] : '');

            $prices->setDate(new \DateTime(isset($fundCenter['fund']['prices'][3]) ? $fundCenter['fund']['prices'][3] : 'NOW'));
            $prices->setYield(isset($fundCenter['fund']['prices'][4]) ? $fundCenter['fund']['prices'][4] : '');
            $prices->setCiticode(isset($fundCenter['fund']['prices'][5]) ? $fundCenter['fund']['prices'][5] : '');
            $prices->setCedol(isset($fundCenter['fund']['prices'][6]) ? $fundCenter['fund']['prices'][6] : '');
            $prices->setIcin(isset($fundCenter['fund']['prices'][7]) ? $fundCenter['fund']['prices'][7] : '');

            $info->setLegalStructure(isset($fundCenter['fund']['additionalInformation']['firstTable'][0]) ? $fundCenter['fund']['additionalInformation']['firstTable'][0] : '');
            $info->setIsaOwn(isset($fundCenter['fund']['additionalInformation']['firstTable'][1]) ? $fundCenter['fund']['additionalInformation']['firstTable'][1] : 0);
            $info->setTrustee(isset($fundCenter['fund']['additionalInformation']['firstTable'][2]) ? $fundCenter['fund']['additionalInformation']['firstTable'][2] : '');
            $info->setRegistrar(isset($fundCenter['fund']['additionalInformation']['firstTable'][3]) ? $fundCenter['fund']['additionalInformation']['firstTable'][3] : '');
            $info->setMultiManager(isset($fundCenter['fund']['additionalInformation']['firstTable'][4]) ? $fundCenter['fund']['additionalInformation']['firstTable'][4] : 0);

            $info->setDividendPolicy(isset($fundCenter['fund']['additionalInformation']['secondTable'][0]) ? $fundCenter['fund']['additionalInformation']['secondTable'][0] : '');
            $info->setFundSize(isset($fundCenter['fund']['additionalInformation']['secondTable'][1]) ? $fundCenter['fund']['additionalInformation']['secondTable'][1] : '');
            $info->setUnitLaunch(new \DateTime( isset($fundCenter['fund']['additionalInformation']['secondTable'][2]) ? $fundCenter['fund']['additionalInformation']['secondTable'][2] : 'now'));
            $info->setFundLaunch(new \DateTime(isset($fundCenter['fund']['additionalInformation']['secondTable'][3]) ? $fundCenter['fund']['additionalInformation']['secondTable'][3] : 'now'));

            $info->setSavingsPlan(isset($fundCenter['fund']['additionalInformation']['thirdTable'][0]) ? $fundCenter['fund']['additionalInformation']['thirdTable'][0] : 0);
            $info->setPricingTimes(isset($fundCenter['fund']['additionalInformation']['thirdTable'][1]) ? $fundCenter['fund']['additionalInformation']['thirdTable'][1] : '');
            $info->setDealingTimes(isset($fundCenter['fund']['additionalInformation']['thirdTable'][2]) ? $fundCenter['fund']['additionalInformation']['thirdTable'][2] : '');
            $info->setDealingFrequency(isset($fundCenter['fund']['additionalInformation']['thirdTable'][3]) ? $fundCenter['fund']['additionalInformation']['thirdTable'][3] : '');

            $fund->setContent($content);
            $fund->setInvestment($investment);
            $fund->setInformation($info);
            $fund->setPrices($prices);
            $fund->setParsed(1);
            $fund->setError(0);

            $this->em->persist($content);
            $this->em->persist($investment);
            $this->em->persist($info);
            $this->em->persist($prices);
            $this->em->persist($fund);

            $this->savePerformance($fund, $data[1]['performance'], $this->checkType($data));

            if (!$this->checkType($data)) {
                $this->saveDividends($fund, $data[2]['dividends']);
                $this->saveMirrors($fund, $data[4]['mirrors']);
                $this->saveBreakdown($fund, $data[3]['breakdown']);
            }

        }

        $this->em->flush();
    }

    public function checkType(array $data)
    {
        return (isset($data['type']) && 'none' === $data['type']) ? true : false;
    }

    public function saveBreakdown($fund, $data)
    {
        $query = $this->em->createQuery('DELETE AppBundle:Breakdown p WHERE p.fund = ' .$fund->getId());
        $query->execute();

        foreach ($data['holdings']['data'] as $break) {
            $b = new Breakdown();
            $b->setName($break[0]);
            $b->setType('holdings');
            $b->setRank($break[1]);
            $b->setPreviousRank((int)$break[2]);
            $b->setPercentage((int)$break[3]);
            $b->setFund($fund);

            $this->em->persist($b);
        }

        foreach ($data['asset']['data'] as $break) {
            $b = new Breakdown();
            $b->setName($break[0]);
            $b->setType('asset');
            $b->setRank($break[1]);
            $b->setPreviousRank((int)$break[2]);
            $b->setPercentage((int)$break[3]);
            $b->setFund($fund);

            $this->em->persist($b);
        }

        foreach ($data['regions']['data'] as $break) {
            $b = new Breakdown();
            $b->setName($break[0]);
            $b->setType('regions');
            $b->setRank($break[1]);
            $b->setPreviousRank((int)$break[2]);
            $b->setPercentage((int)$break[3]);
            $b->setFund($fund);

            $this->em->persist($b);
        }

        foreach ($data['sectors']['data'] as $break) {
            $b = new Breakdown();
            $b->setName($break[0]);
            $b->setType('sectors');
            $b->setRank($break[1]);
            $b->setPreviousRank((int)$break[2]);
            $b->setPercentage((int)$break[3]);
            $b->setFund($fund);

            $this->em->persist($b);
        }
    }


    public function saveMirrors($fund, $data)
    {
        $query = $this->em->createQuery('DELETE AppBundle:Mirrors p WHERE p.fund = ' .$fund->getId());
        $query->execute();

        foreach ($data['life'] as $mirror) {
            $m = new Mirrors();
            $m->setName($mirror[0]);
            $m->setAnnualCharge((float)$mirror[1]);
            $m->setFundSize((float)$mirror[2]);
            $m->setFund($fund);
            $m->setType('life');

            $this->em->persist($m);
        }

        foreach ($data['pension'] as $mirror) {
            $m = new Mirrors();
            $m->setName($mirror[0]);
            $m->setAnnualCharge((float)$mirror[1]);
            $m->setFundSize((float)$mirror[2]);
            $m->setFund($fund);
            $m->setType('pension');

            $this->em->persist($m);
        }
    }

    public function saveDividends($fund, $data)
    {
        $query = $this->em->createQuery('DELETE AppBundle:Dividends p WHERE p.fund = ' .$fund->getId());
        $query->execute();

        foreach ($data as $dividend) {
            $d = new Dividends();
            $d->setType($dividend[0]);
            $d->setAmount($dividend[1]);
            $d->setTax($dividend[2]);
            $d->setDividendDate(new \DateTime($dividend[3]));
            $d->setPaymentDate(new \DateTime(($dividend[4])));
            $d->setFund($fund);

            $this->em->persist($d);
        }

    }

    public function savePerformance($fund, $data, $type)
    {
        $query = $this->em->createQuery('DELETE AppBundle:Performance p WHERE p.fund = ' .$fund->getId());
        $query->execute();

        $p1 = new Performance();
        $p1->setName($data['quarter-end']['name']);
        $p1->setFund($fund);
        $p1->setData(serialize($data['quarter-end']));
        $this->em->persist($p1);

        if (!$type) {
            $p2 = new Performance();
            $p2->setName($data['rolling']['name']);
            $p2->setFund($fund);
            $p2->setData(serialize($data['rolling']));

            $p3 = new Performance();
            $p3->setName($data['year']['name']);
            $p3->setFund($fund);
            $p3->setData(serialize($data['year']));

            $this->em->persist($p2);
            $this->em->persist($p3);
        }
    }
}
