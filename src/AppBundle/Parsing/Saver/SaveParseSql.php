<?php

namespace AppBundle\Parsing\Saver;

use Doctrine\ORM\EntityManager;

/**
 * Class SaveParseSql
 * @package AppBundle\Parsing\Saver
 */
class SaveParseSql
{
    /**
     * @var array
     */
    protected $countersArr = array();

    /**
     * @var array
     */
    protected $queriesArr = array();

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Type of entities
     *
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $update;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Init class variables
     *
     * @param string $type
     * @param string $update
     */
    public function startSql($type, $update)
    {
        $this->type = $type;
        $this->update = $update;
        $this->refresh();
    }

    /**
     * Return full table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->type;
    }

    /**
     * Add Data to SQL query
     *
     * @param array $dataArr
     */
    public function addSql(array $dataArr)
    {
        if ($this->countersArr[$this->type] > 100) {
            $this->finishQuery($this->type, $this->update);

            $this->refresh();
        }

        if ($this->queriesArr[$this->type] == '') {
            $this->queriesArr[$this->type] = " INSERT INTO ".$this->getTableName()
                . "(". implode(',', array_keys($dataArr)). ") VALUES";
        }

        if ($this->countersArr[$this->type] > 0) {
            $this->queriesArr[$this->type] .= ",";
        }

        $this->queriesArr[$this->type] .= $this->prepareQuery($dataArr);

        $this->countersArr[$this->type] = intval($this->countersArr[$this->type], 10) + 1;
    }

    /**
     * Finish SQL
     */
    public function finishSql()
    {
        foreach ($this->countersArr as $type => $counterValue) {
            if ($counterValue > 0) {
                $this->finishQuery($type);
            }
            $this->refresh();
        }
    }

    /**
     * Finish query
     *
     * @param $type
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function finishQuery($type)
    {
        if ($this->queriesArr[$type]) {
            $this->queriesArr[$type] .= " ON DUPLICATE KEY UPDATE ".str_replace("%value%", $this->update, "%value%=VALUES(%value%)");
        }

        $this->em->getConnection()->executeQuery($this->queriesArr[$type]);
    }

    /**
     * Refresh class variables
     */
    private function refresh()
    {
        $this->countersArr[$this->type] = 0;
        $this->queriesArr[$this->type] = '';
    }

    /**
     * Formate string query by given array
     *
     * @param array $dataArr
     * @return string
     */
    private function prepareQuery(array $dataArr)
    {
        $query = '(';
        foreach ($dataArr as $key => $value) {
            if (in_array($key, array('code', 'created_at'))) {
                $query .= '"'.$value.'",';
            } else {
                $query .= $value.',';
            }
        }

        return substr($query, 0, -1).')';
    }
}
