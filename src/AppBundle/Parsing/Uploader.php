<?php

namespace AppBundle\Parsing;

use AppBundle\Entity\Images;
use AppBundle\Entity\News;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use AppBundle\S3\AmazonS3Helper;

/**
 * Class Uploader
 * @package AppBundle\Parsing
 */
class Uploader
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var News
     */
    private $news;

    /**
     * @var Filesystem
     */
    protected $fs;

    /**
     * @var $host
     */
    protected $host;

    /**
     * @var string S3|system
     */
    protected $method;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var AmazonS3Helper;
     */
    protected $s3;

    /**
     * @param EntityManager      $em
     * @param ContainerInterface $container
     * @param AmazonS3Helper     $s3
     */
    public function __construct(EntityManager $em, ContainerInterface $container, AmazonS3Helper $s3)
    {
        $this->em = $em;
        $this->container = $container;

        $this->host = $container->getParameter('host');
        $this->method = $container->getParameter('upload_method');

        $this->fs = new Filesystem();
        $this->s3 = $s3;
    }

    /**
     * @param News $news
     */
    public function setNews(News $news)
    {
        $this->news = $news;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Run Uploader
     *
     * @param array $images
     * @return array
     */
    public function run(array $images)
    {
        $webDir = 'web/';
        $imgPath = 'images/'.$this->getNews()->getId();
        $imagesDir = dirname(__FILE__).'/../../../'.$webDir;

        // Rebuild this code to strategy upload method injection
        if ('S3' === $this->method) {
            $s3 = $this->s3->getClient();
        }

        if (!$this->fs->exists($webDir.$imgPath)) {
            try {
                $this->fs->mkdir($webDir.$imgPath);
            } catch (IOExceptionInterface $e) {
                echo "An error occurred while creating img directory at ".$e->getPath();
            }
        }

        $pathArray = array();
        foreach ($images as $key => $img) {
            $targetPath = $imgPath.'/'.$key.'.'.$this->getType($img);

            $this->fs->copy($img, $imagesDir.$targetPath);

            if (isset($s3)) {
                $result = $this->s3->upload($targetPath, $imagesDir.$targetPath);
            }

            $newsImage = new Images();

            $newsImage->setSrc(isset($s3) ? $result['ObjectURL'] : $this->host.'/'.$targetPath);
            $newsImage->setNews($this->getNews());

            $this->em->persist($newsImage);

            $pathArray[] = isset($s3) ? $result['ObjectURL'] : $this->host.'/'.$targetPath;
        }
        $this->em->flush();

        if (isset($s3)) {
            $this->removeTmpFiles($this->getNews()->getId(), $imagesDir);
        }

        return $pathArray;
    }

    /**
     * @param $img
     * @return mixed
     */
    private function getType($img)
    {
        $pathInfo = pathinfo($img);

        return $pathInfo['extension'];
    }

    /**
     * Helper for delete tmp files
     *
     * @param int    $id
     * @param string $path
     */
    private function removeTmpFiles($id, $path)
    {
        $this->em->getRepository('AppBundle:News')->removeTmpFiles($path.'images/'.$id);
    }
}
