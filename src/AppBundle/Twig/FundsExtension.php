<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Fund;
use AppBundle\Entity\User;
use AppBundle\Entity\News;
use Symfony\Component\Routing\Router;

/**
 * Class PusherExtension
 * @package AppBundle\Twig
 */
class FundsExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'parseNewsStatus' => new \Twig_Function_Method($this, 'parseNewsStatus'),
            'parseStatus' => new \Twig_Function_Method($this, 'parseStatus'),
            'parseType' => new \Twig_Function_Method($this, 'parseType'),
            'createUpDown' =>  new \Twig_Function_Method($this, 'createUpDown'),
            'sendingStatus' =>  new \Twig_Function_Method($this, 'sendingStatus'),
        );
    }

    /**
     * @param Fund $fund
     * @return string
     */
    public function parseStatus(Fund $fund)
    {
        if ($fund->getError()) {
            $label = 'danger';
            $text = 'Has Error';
        } else {
            $label = $fund->getParsed() ? 'success' : 'default';
            $text = $fund->getParsed() ? 'OK' : 'Waiting';
        }

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param Fund $fund
     * @return string
     */
    public function parseType(Fund $fund)
    {
        $label = $fund->getOur() ? 'success' : 'warning';
        $text = $fund->getOur() ? 'Our' : 'Trustnet';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     */
    public function parseNewsStatus(News $news)
    {
        $label = $news->getParsed() ? 'success' : 'default';
        $text =  $news->getParsed() ? 'OK' : 'Waiting';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     * @return string
     */
    public function sendingStatus(News $news)
    {
        $label = $news->getSending() ? 'success' : 'default';
        $text =  $news->getSending() ? 'OK' : 'Waiting';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     */
    public function createUpDown(News $news)
    {
        $upPath = $this->router->generate("funds_admin_up_news", array('id' => $news->getId()));
        $downPath = $this->router->generate("funds_admin_down_news", array('id' => $news->getId()));

        $result = str_replace(
            array('%pathUp%', '%pathDown%'),
            array($upPath, $downPath),
            '<a href="%pathUp%" title="Up"><span class="glyphicon glyphicon-arrow-up"></span></a>'.
            '<a href="%pathDown%" title="Down"><span class="glyphicon glyphicon-arrow-down"></span></a>'
        );

        echo $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'funds_extension';
    }
}